#define maswidth 5
#define masheight 5

#include <iostream>
#include <math.h>

using namespace std;

int main(){
    
    long double b, a, h, y, f;
    long long sum = 0;
    int mas[masheight][maswidth] = {    {   3,  45,  32, 445,  34},
                                        {  47, -22,   0, 554,  -3},
                                        {-543, 559,  21,   0,  -7},
                                        {-443,  77,  85, 733,   7},
                                        {   1,   2,   3,   4, 666}   };
    double mid[maswidth];
    
    for (int i = 0; i < maswidth; i++)
    {
        for (int j = 0; j < masheight; j++)
        {
            
//            cout << "Enter element number " << i + 1 << ", " << j + 1 << endl;            // Можно с клавиатуры задать массив
//            cin >> mas[i][j];
            
            sum += mas[i][j];
    
        }
        mid[i] = (long double) sum / maswidth;
        
    } 
    
    for (long double i : mid)
    {
        cout << i << " ";
    }
    
    cout << endl;
    
    return 0;
    
}
