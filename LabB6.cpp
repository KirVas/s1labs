#define a -0.1
#define b 1
#define et 10e-5

#include <iostream>
#include <math.h>

using namespace std;


long double yfunc(double x);
long double sfunc(double x, int *it);

long double yfunc(double x)
{ 
    long double rt = pow(2, -1 * x);
    return rt;
}

long double sfunc(double x, int *it)
{ 
    double slg = -1 * log(2); 
    double mng = 1;
    long double sum = 0;
    double n = 1;
    do
    {
        sum += mng;
        mng *= x * slg / n;
        n++;
        *it = *it + 1;
    } while (fabs(mng) > et);
    return sum;
}

int main(){
    
     
    long double s, t, h, y, f, pw;
    int it = 0;
    
    h = (b - a) / 10.;
    
    for (double x = a; x < b; x = x + h)
    {
        y = yfunc(x);
        s = sfunc(x, &it); 
        cout << "y(x) = " << y << ", s(x) = " << s << " with " << it <<" iterations, x = " << x << endl;
        it = 0;
    }

    return 0;
}
