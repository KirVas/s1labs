#include <iostream>
#include <math.h>
#include <cstdlib>
using namespace std;

int main(){
    
    double b, a, h, y, f, tel;
    long long sum = 0, maxsum = 0, lineindex = 0;
    int masheight, maswidth;
    long long elmax, eljmax, elimax, elmin, eljmin, elimin;
    
    cout << "Enter array height" << endl;
    cin >> masheight;
    cout << "Enter array width" << endl;
    cin >> maswidth;
    
    double **mas = new double*[masheight];
    for (int i = 0; i < masheight; i++)
    {
        mas[i] = new double[maswidth];
    }
    
    for (int i = 0; i < masheight; i++)
    {
        for (int j = 0; j < maswidth; j++)
        {
            
//            cout << "Enter element number " << i + 1 << ", " << j + 1 << endl;
//            cin >> mas[i][j];
            mas[i][j] = rand() * rand() % 1000;
            cout << mas[i][j] << " ";
    
        }
        cout << endl;        
    } 
    
    elimin = 0;
    elimax = 0;
    eljmin = 0;
    eljmax = 0;
    elmin = mas[0][0];
    elmax = mas[0][0];
    
    for(int i=0; i < masheight; i++)
    {
        for(int j =0; j < maswidth; j++)
        {
            if (mas[i][j] > elmax)
            {
                elmax = mas[i][j];
                eljmax = j;
                elimax = i;
            }
            if (mas[i][j] < elmin)
            {
                elmin = mas[i][j];
                eljmin = j;
                elimin = i;
            }
        }
    }
    
    cout << endl << "min = " << elmin << " max = " << elmax << endl << endl;
 
    
    for (int j = 0; j < maswidth; j++)
    {
        tel = mas[elimax][j];
        mas[elimax][j] = mas[elimin][j];
        mas[elimin][j] = tel;
    }
    
    for (int i = 0; i < masheight; i++)
    {
        for (int j = 0; j < maswidth; j++)
        {
            cout << mas[i][j] << " ";
    
        }
        cout << endl;        
    } 
    
    for (int i = 0; i < masheight; i++)
    {
        delete []mas[i];
    }
    delete []mas;
    mas = NULL;
    
    return 0;
    
}
