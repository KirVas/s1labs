#include <iostream>
#include <cstdio>

using namespace std;

int main(){
    
    char s[1];
    double g,l;
    cout << "To convert gallons to liters enter 0, otherwise enter 1" << endl;
    fgets(s);
    switch(s[1])
    {
        case '0':
        {
            cout << "Enter the number of gallons" << endl;
            cin >> g;
            l = g * 3.785;
            cout << g << " gallons = " << l << " liters." << endl;
            break;
        }
        case '1':
        {
            cout << "Enter the number of liters" << endl;
            cin >> l;
            g = l / 3.785;
            cout << l << " liters = " << g << " gallons." << endl;
            break;
        }
        default:
        {
            cout << "Unexpected symbol. Exiting..." << endl;
            break;
        }
        
    }
    
    return 0;
}
