#define n 5

#include <iostream>
#include <math.h>
#include <string>

using namespace std;

struct passanger {
    int id;
    string name;
    int bilet;
    int ves;
} *list;

int main()
{
    list = new passanger[n];
    
    list[0].id = 1;
    list[0].name = "Abadanga";
    list[0].bilet = 45656;
    list[0].ves = 100002;
    
    list[1].id = 2;
    list[1].name = "GJman";
    list[1].bilet = 5564478;
    list[1].ves = 999;
 
    list[2].id = 3;
    list[2].name = "Ishlanbek";
    list[2].bilet = 6757;
    list[2].ves = 193888;
       
    list[3].id = 4;
    list[3].name = "Vasov";
    list[3].bilet = 78548;
    list[3].ves = 55601;
 
    list[4].id = 5;
    list[4].name = "Petrov";
    list[4].bilet = 6453347;
    list[4].ves = 2442;

    passanger temp;
    
    for (int i = 0; i < n - 1; i++){
        for(int j = i + 1; j < n; j++){
            if (list[i].ves <= 15000 && list[j].ves <= 15000  && list[i].name > list[j].name){
                temp = list[i];
                list[i] = list[j];
                list[j] = temp;
            }
        }
    }
    
    for (int i = 0; i < n; i++){
        if (list[i].ves <= 15000){
            cout << list[i].name << "'s ticket number is " << list[i].bilet << " and his bag is " << list[i].ves << " g" << endl;
            
        }
    }
    
    
    delete []list;
    return 0;
    

}

