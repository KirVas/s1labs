#include <iostream>
#include <math.h>
#include <string>
#include <stdlib.h>
#include <ctype.h>
#include <cstring>

using namespace std;

int main()
{
    long sum;
    char s[1000000];
    cout << "Enter your string" << endl;
    cin >> s;
    
    
    for (int i = 0; i < strlen(s); i++)
    {
        if (isdigit(s[i]))
        {
            sum += int (s[i]) - 48;
        }
    }
    
    cout << "Digit sum: " << sum << endl;
    
    
    return 0;
}
