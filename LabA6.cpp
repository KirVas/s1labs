#define a 0.22
#define b 1.1
#define n 11

#include <iostream>
#include <math.h>

using namespace std;


long double func(double x, int nt)
{ 
    long double sum = 0;
    for (int i = 1; i < nt; i++)
    {
        sum += 2 * cos (i * x) * cosh(x);
    }
    return sum;
}


int main(){
    
     
    long double s, t, h, y, f, pw;
    
    h = (b - a) / 10.;
    
    for (double x = a; x < b; x = x + h)
    {
        y = func(x, n);
        cout << "y(x, n) = " << y << endl;
        

    }

    return 0;
}
