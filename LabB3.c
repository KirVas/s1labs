#define a 0.1
#define b 1
#define k 140

#include <iostream>
#include <math.h>

using namespace std;

int main(){
    
     
    double s, n, h, y, f, pw;
    long fct;
    short t;
    
    h = (b - a) / 10.;
    
    for (double x = a; x < b; x = x + h)
    {
        y = sin(x) / x;
        fct = 1;
        pw = 1;
        s = 0;
        f = 1;
        for (int n = 0; n <= k; n++)
        {
            f *= -1 * pw / fct;
            s += f;
            pw = x * x;
            fct = 2 * (n + 1);
            fct *= 2 * (n + 1) + 1;

        }
        s *= -1;
        cout << "y(x) = " << y << ", s(x) = " << s << ", x = " << x << endl;

    }

    return 0;
}
