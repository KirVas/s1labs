#include <iostream>
#include <math.h>

using namespace std;

int func(double x, int *t)
{
    cout << *t;
    *t = *t + 1;
    cout << *t;
    return x * 2;
}

int main()
{
    int t = 0; //new int;
    double x =1;
    
    x = func(x,&t);
    x = func(x,&t);
    
    cout << x << " " << t;
    
    return 0;
    

}
