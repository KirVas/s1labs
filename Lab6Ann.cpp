#include <iostream>
#include <math.h>
#include <iomanip>
using namespace std;
double y (double *);
double r (double *, double *);
int main()
{
	double a, b, h, eps, x;
    int n;
	cout << "Vvedite a: "; cin >> a;
	cout << "Vvedite b: "; cin >> b;
	cout << "Vvedite eps: "; cin >> eps;
	h = (b - a) / 10;
	for (x = a; x < b + h / 2; x += h)
	{
		cout << "x=" << x << setw(10) << "y=" << y(&x) << setw(10) << "s=" << r(&x, &eps)<<endl;
	}
	system("pause");
    return 0;
	
}

double y(double *x)
{
	return sin(*x)*sin(*x);
}

double r(double *x, double *eps)
{
    int n;
    double p,s;
    p=*x*(*x);
    s=p;
    n=2;
	while (fabs(p) > *eps)
	{
        p*=-4*(*x)*(*x)/((2*n)*(2*n-1));
        s+=p;
        n++;
    }
    return(-s);
}
