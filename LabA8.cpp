#include <iostream>
#include <math.h>
#include <string>

using namespace std;


struct country {
    int id;
    string name;
    string capital;
    int square;
    string geolocation;
};

int main()
{
    
    country list[5];
    
    list[0].id = 1;
    list[0].name = "Abadanga";
    list[0].capital = "Barumba";
    list[0].square = 10002;
    list[0].geolocation = "Africa";
    
    list[1].id = 2;
    list[1].name = "Australia";
    list[1].capital = "Kanberra";
    list[1].square = 999;
    list[1].geolocation = "Australia";
 
    list[2].id = 3;
    list[2].name = "US";
    list[2].capital = "Washington";
    list[2].square = 193888;
    list[2].geolocation = "America";
       
    list[3].id = 4;
    list[3].name = "Egypt";
    list[3].capital = "Kair";
    list[3].square = 55601;
    list[3].geolocation = "Africa";
    
    list[4].id = 5;
    list[4].name = "Canada";
    list[4].capital = "Minsk";
    list[4].square = 2442;
    list[4].geolocation = "America";
    
    for (country c : list){
        if (c.geolocation == "Africa"){
            cout << c.name << "'s capitals name is " << c.capital << " and its area is " << c.square << " km^2" << endl;
            
        }
    }
    
    return 0;
    

}

