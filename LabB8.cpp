#define n 5

#include <iostream>
#include <math.h>
#include <string>

using namespace std;

struct country {
    int id;
    string name;
    string capital;
    int square;
    string geolocation;
} *list;

int main()
{
    list = new country[n];
    
    list[0].id = 1;
    list[0].name = "Abadanga";
    list[0].capital = "Barumba";
    list[0].square = 100002;
    list[0].geolocation = "Africa";
    
    list[1].id = 2;
    list[1].name = "Australia";
    list[1].capital = "Kanberra";
    list[1].square = 999;
    list[1].geolocation = "Australia";
 
    list[2].id = 3;
    list[2].name = "US";
    list[2].capital = "Washington";
    list[2].square = 193888;
    list[2].geolocation = "America";
       
    list[3].id = 4;
    list[3].name = "Egypt";
    list[3].capital = "Kair";
    list[3].square = 55601;
    list[3].geolocation = "Africa";
    
    list[4].id = 5;
    list[4].name = "Canada";
    list[4].capital = "Minsk";
    list[4].square = 2442;
    list[4].geolocation = "America";
    
    country temp;
    
    for (int i = 0; i < n - 1; i++){
        for(int j = i + 1; j < n; j++){
            if (list[i].geolocation == "Africa" && list[j].geolocation == "Africa" && list[i].square > list[j].square){
                temp = list[i];
                list[i] = list[j];
                list[j] = temp;
            }
        }
    }
    
    for (int i = 0; i < n; i++){
        if (list[i].geolocation == "Africa"){
            cout << list[i].name << "'s capitals name is " << list[i].capital << " and its area is " << list[i].square << " km^2" << endl;
            
        }
    }
    
    
    delete []list;
    return 0;
    

}

