#include <iostream>

using namespace std;

int main(){
    
    long double sum, mid, b, a, ml, mn;
    int i, n, maslength, y, f, h, p;
    char c;
    
    cout << "Enter array size" << endl;
    cin >> maslength;
    
    char mas[maslength] = { 'b', 'z', 'a', 'h', 'a', 't', 'h', 't', 'i', 'n' };
    
   /* for (int i = 0; i < maslength; i++){                                  // Возможность ввести массив
        cout << "Enter element number " << i << endl;
        cin >> mas[i];
    } */
   
   if (maslength == 1)
   {
       cout << mas[0] << endl;
       return 0;
   }
   
   for (int i = 0; i < maslength; i++)                                      // Сортировка
   {
        for (int j = i + 1; j < maslength; j++)
        {
            if (mas[i] > mas[j])
            {           
                c = mas[i];
                mas[i] = mas[j];
                mas[j] = c;
            }
        }
   }
   
   if (mas[0] != mas[1])
   {
       cout << mas[0];
   }
   
   for (int i = 1; i < maslength - 1; i++)
   {
       if ((mas[i] != mas[i - 1]) && (mas[i] != mas[i + 1]))
       {
            cout << mas[i];
       }
   }
   
   if (mas[maslength - 1] != mas[maslength - 2])
   {
        cout << mas[maslength - 1];
   }
   

}
