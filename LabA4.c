#define maslength 10

#include <iostream>
#include <math.h>

using namespace std;

int main(){
    
    double mas[maslength] = { 23, 435, -2.1, 44.24, 54343, -2.2345, -43334, -235.563, 4454, 233 };
    double sum, mid, b, a, h, y, f;
    
    /* for (int i = 0; i < maslength; i++){
        cout << "Enter element number " << i << endl;            // Можно с клавиатуры задать массив
        cin >> mas[i];
    }  */
    sum = 0;                                                  // Если не обнулить вот эти две переенные, они начинают шутки шутить. 
    mid = 0;
    for (int i = 0; i < maslength; i++){
        sum += mas[i];                                               // Расчёт среднего значения 19-22 
    }
    mid = sum / maslength;
    for (double i : mas)
    {
        if (i > mid)
        {
            cout << i << endl;                                        // Вывод среднего
        }
    }
    return 0;
    
}
