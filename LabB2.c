#define _USE_MATH_DEFINES

#include <iostream>
#include <math.h>
#include <string>

using namespace std;

int main(){
    
    int ch;
    char s;
    double f, x, y, e;
    string line;
    
    cout << "Enter x value:" << endl;
    cin >> x;
    cout << "Enter y value:" << endl;
    cin >> y;
    
    cout << "Choose the function you want:" << endl;
    cout << "1 to use sh(x), A to use x^2, 3 or to use e^x" << endl;
    cin >> s;
    
    cout << "You've chosen f(x) = ";
    switch(s){
        case '1':
        {
            f = sinh(x);
            cout << "sh(x)" << endl;
            break;
        }
        case 'A':
        {
            cout << "x^2" << endl;
            f = x*x;
            break;
        }
        case '3':
        {
            cout << "e^x" << endl;
            f = exp(x);
            break;
        }
        default:
        {
            cout << "other function. Exiting..." << endl;
            return 1;
        }
        
    }
    
    
    if (x * y == 0)
    {
        e = sin(f)/3;
        line = "xy = 0";
    }
    else if (x * y > 7 || x * y < 10) 
    {
        e = log(fabs(y-f));
        line = "7 < xy < 10";
    }
    else
    {
        int sq = tan(x);
        e = 2 * sq * sq - y;
        line = "that is not special case";
    }
        
    cout << "e = " << e << " because " << line << endl;
    return 0;
    
}
