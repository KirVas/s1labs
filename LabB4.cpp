#include <iostream>
#include <math.h>

using namespace std;

int main(){
    
    long double sum, mid, b, a, ml, mn;
    int i, n, maslength, y, f, h, p;
    
    cout << "Enter array size" << endl;
    cin >> maslength;
    
    double mas[maslength] = { 23, 435, -2.1, 44.24, 54343, -2.2345, -43334, -235.563, 4454, 233 };
    
   /* for (int i = 0; i < maslength; i++){
        cout << "Enter element number " << i << endl;
        cin >> mas[i];
    } */
   
    cout << "Enter shift size" << endl;
    cin >> n;
    
    y = 0;
    p = maslength / 2;

    n = n % maslength;
    maslength--;
    
    if (n < 0) 
    { 
        n = maslength + n;
    } else if (n == 0)
    {
        for (double p : mas)
        {
            cout << p << " ";
        }
        cout << endl;

        return 0;
    }
    
    h = (maslength - n)/2;
    f = maslength - n/2;
    
    for (int i = 0; i <= h; i++)
    {
        mn = mas[i];
        mas[i] = mas[maslength - n - i];
        mas[maslength - n - i] = mn;
    }
    
    for (int i = maslength - n + 1; i <= f; i++)
    {
        mn = mas[i];
        mas[i] = mas[maslength - y];
        mas[maslength - y] = mn;
        y++;
    }
    
    for (int i = 0; i < p; i++)
    {
        mn = mas[i];
        mas[i] = mas[maslength - i];
        mas[maslength - i] = mn;
    }
    
    for (double p : mas)
    {
        cout << p << " ";
    }
    
    cout << endl;

    return 0;
    
}
