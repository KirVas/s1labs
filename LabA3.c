#include <iostream>
#include <math.h>

using namespace std;

int main(){
    
    
    double n, k, b, a, h, y, f;
    cout << "Enter a" << endl;
    cin >> a;
    cout << "Enter b" << endl;
    cin >> b;
    cout << "Enter n" << endl;
    cin >> n;
    
    h = (b - a) / 10.;
    
    
    for (double x = a; x < b; x = x + h)
    {
        for (int i  = 1; i <= n; i++)
        {
    
            y += 2. * tan(i * x) * exp(2 * i);
        }
        cout << "y(x) = " << y << ", x = " << x << endl;
        
    }
    
    return 0;
}
