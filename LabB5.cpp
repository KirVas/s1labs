#include <iostream>
#include <math.h>
#include <cstdlib>
using namespace std;

int main(){
    
    long double b, a, h, y, f;
    long long sum = 0, maxsum = 0, lineindex = 0;
    int masheight, maswidth;
    
    cout << "Enter array height" << endl;
    cin >> masheight;
    cout << "Enter array width" << endl;
    cin >> maswidth;
    
    double **mas = new double*[masheight];
    for (int i = 0; i < masheight; i++)
    {
        mas[i] = new double[maswidth];
    }
    
    for (int i = 0; i < masheight; i++)
    {
        for (int j = 0; j < maswidth; j++)
        {
            
//            cout << "Enter element number " << i + 1 << ", " << j + 1 << endl;
//            cin >> mas[i][j];
            mas[i][j] = rand() % 100 * rand() % 10;
            cout << mas[i][j] << " ";
            sum += mas[i][j];
    
        }
        cout << endl;
        if (sum > maxsum)
        {
            maxsum = sum;
            lineindex = i;
        }
        sum = 0;
        
    } 
    
    cout << endl << endl;
    
    for (int i = lineindex + 1; i < masheight; i++)
    {
             for (int j = 0; j < maswidth; j++)
             {
                mas[i-1][j] = mas[i][j];
             }
    }
    
    masheight--;
    delete []mas[masheight];
    
    for (int i = 0; i < masheight; i++)
    {
             for (int j = 0; j < maswidth; j++)
             {
                cout << mas[i][j] << " ";
             }
             cout << endl;
    }
    
    for (int i = 0; i < masheight; i++)
    {
        delete []mas[i];
    }
    delete []mas;
    mas = NULL;
    
    return 0;
    
}
